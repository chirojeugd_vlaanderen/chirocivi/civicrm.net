﻿/*
  Copyright 2015, 2016, 2018 Chirojeugd-Vlaanderen vzw

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

using System;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Chiro.CiviCrm.Api.DataContracts.Requests
{
    /// <summary>
    /// A CiviCRM participant request.
    /// </summary>
    [CiviRequest]
    public partial class ParticipantRequest : BaseRequest
    {

        [JsonProperty("contact_id", NullValueHandling = NullValueHandling.Ignore)]
        public int? ContactId { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty("contact_type", NullValueHandling = NullValueHandling.Ignore)]
        public ContactType? ContactType { get; set; }

        [JsonProperty("event_id", NullValueHandling = NullValueHandling.Ignore)]
        public int? EventId { get; set; }

        [JsonProperty("participant_role_id", NullValueHandling = NullValueHandling.Ignore)]
        public DeelnameRol? ParticipantRole { get; set; }

        [JsonProperty("participant_status_id", NullValueHandling = NullValueHandling.Ignore)]
        public DeelnameStatus? ParticipantStatus { get; set; }

        #region chaining
        [JsonProperty("api.Contact.get", NullValueHandling = NullValueHandling.Ignore)]
        public ContactRequest ContactGetRequest { get; set; }

        [JsonProperty("api.Event.get", NullValueHandling = NullValueHandling.Ignore)]
        public EventRequest EventGetRequest { get; set; }
        #endregion

        public override CiviEntity EntityType
        {
            get { return CiviEntity.Participant; }
        }
    }
}
