﻿using Chiro.CiviCrm.Api.Converters;
using Chiro.CiviCrm.Api.DataContracts.Filters;

using Newtonsoft.Json;

namespace Chiro.CiviCrm.Api.DataContracts.Requests
{
    public class ContactGetRequest : ContactRequest
    {
        [JsonConverter(typeof(FilterConverter))]
        [JsonProperty("external_identifier", NullValueHandling = NullValueHandling.Ignore)]
        public IFilter ExternalIdentifierFilter { get; set; }
    }
}
