﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Chiro.CiviCrm.Api.DataContracts.Requests
{
    [DataContract]
    [CiviRequest]
    public partial class FormProcessorRequest : BaseRequest
    {
        public override CiviEntity EntityType
        {
            get { return CiviEntity.FormProcessor; }
        }
    }
}
