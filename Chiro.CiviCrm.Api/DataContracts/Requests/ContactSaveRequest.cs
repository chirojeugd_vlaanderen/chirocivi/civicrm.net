﻿using Newtonsoft.Json;

namespace Chiro.CiviCrm.Api.DataContracts.Requests
{
    public class ContactSaveRequest : ContactRequest
    {
        [JsonProperty("external_identifier", NullValueHandling = NullValueHandling.Ignore)]
        public string ExternalIdentifier { get; set; }
    }
}
