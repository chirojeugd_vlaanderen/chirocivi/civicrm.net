﻿/*
   Copyright 2023 Chirojeugd-Vlaanderen vzw

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

using Chiro.CiviCrm.Api.Converters;

using Newtonsoft.Json;

using System;

namespace Chiro.CiviCrm.Api.DataContracts.Requests
{
    public partial class FormProcessorRequest : BaseRequest
    {
        [JsonProperty("start_date", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateConverter))]
        public DateTime? StartDate { get; set; }

        [JsonProperty("end_date", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? EndDate { get; set; }

        [JsonProperty("lid_contact_id", NullValueHandling = NullValueHandling.Ignore)]
        public int LidContactId { get; set; }

        [JsonProperty("ploeg_contact_id", NullValueHandling = NullValueHandling.Ignore)]
        public int PloegContactId { get; set; }

        [JsonProperty("factuurstatus", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(NullableEnumConverter))]
        public FactuurStatus Status { get; set; }

        [JsonProperty("is_gratis", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(BoolConverter))]
        public bool IsGratis { get; set; }

        [JsonProperty("sociaal_tarief", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(BoolConverter))]
        public bool MetSociaalTarief { get; set; }

        [JsonProperty("extra_verzekering", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(BoolConverter))]
        public bool ExtraVerzekering { get; set; }
    }
}
