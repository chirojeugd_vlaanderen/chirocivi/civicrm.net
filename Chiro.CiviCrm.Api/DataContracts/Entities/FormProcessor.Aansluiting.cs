﻿/*
   Copyright 2023 Chirojeugd-Vlaanderen vzw

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

using Chiro.CiviCrm.Api.Converters;

using Newtonsoft.Json;

using System;
using System.Runtime.Serialization;

namespace Chiro.CiviCrm.Api.DataContracts.Entities
{
    public partial class FormProcessor
    {
        [DataMember(Name = "start_date")]
        public DateTime? StartDate { get; set; }

        [DataMember(Name = "lid_contact_id")]
        public int LidContactId { get; set; }

        [DataMember(Name = "ploeg_contact_id")]
        public int PloegContactId { get; set; }

        [DataMember(Name = "factuurstatus")]
        [JsonConverter(typeof(NullableEnumConverter))]
        public FactuurStatus Status { get; set; }

        [DataMember(Name = "is_gratis")]
        public bool IsGratis { get; set; }

        [DataMember(Name = "sociaal_tarief")]
        public bool MetSociaalTarief { get; set; }
    }
}
