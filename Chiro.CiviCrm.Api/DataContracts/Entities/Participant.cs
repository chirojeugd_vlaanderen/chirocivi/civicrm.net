﻿/*
  Copyright 2014, 2015, 2018 Chirojeugd-Vlaanderen vzw

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

using System;
using System.Runtime.Serialization;

using Chiro.CiviCrm.Api.Converters;

using Newtonsoft.Json;

namespace Chiro.CiviCrm.Api.DataContracts.Entities
{
    /// <summary>
    /// A CiviCRM Participant.
    /// </summary>
    [DataContract]
    [CiviRequest]
    public partial class Participant : IEntity
    {
        [DataMember(Name = "id"), JsonProperty]
        public int Id { get; set; }

        [DataMember(Name = "contact_id"), JsonProperty]
        public int ContactId { get; set; }

        [DataMember(Name = "event_id"), JsonProperty]
        public int EventId { get; set; }

        [DataMember(Name = "event_title"), JsonProperty]
        public string EventTitle { get; set; }

        [DataMember(Name = "event_start_date"), JsonProperty]
        [JsonConverter(typeof(Crm15863Converter))]
        public DateTime? EventStartDate { get; set; }

        [DataMember(Name = "event_end_date"), JsonProperty]
        [JsonConverter(typeof(Crm15863Converter))]
        public DateTime? EventEndDate { get; set; }

        [DataMember(Name = "participant_role_id"), JsonProperty]
        [JsonConverter(typeof(NullableEnumConverter))]
        public DeelnameRol ParticipantRole { get; set; }

        [DataMember(Name = "participant_id"), JsonProperty]
        public int ParticipantId { get; set; }

        [DataMember(Name = "event_type"), JsonProperty]
        public string EventType { get; set; }

        [DataMember(Name = "participant_status_id"), JsonProperty]
        public DeelnameStatus ParticipantStatus { get; set; }

        [DataMember(Name = "is_pay_later"), JsonProperty]
        [JsonConverter(typeof(BoolConverter))]
        public bool? IsPayLater { get; set; }

        [DataMember(Name = "is_test"), JsonProperty]
        [JsonConverter(typeof(BoolConverter))]
        public bool? IsTest { get; set; }

        #region Chaining
        [DataMember(Name = "api.Contact.get")]
        public ApiResultValues<Contact> ContactResult { get; set; }

        [DataMember(Name = "api.Event.get")]
        public ApiResultValues<Event> EventResult { get; set; }
        #endregion
    }
}
