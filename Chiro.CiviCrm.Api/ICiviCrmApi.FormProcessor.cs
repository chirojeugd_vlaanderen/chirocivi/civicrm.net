﻿/*
   Copyright 2023 Chirojeugd-Vlaanderen vzw

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

using Chiro.CiviCrm.Api.DataContracts;
using System.ServiceModel;
using System.ServiceModel.Web;
using Chiro.CiviCrm.Api.DataContracts.Requests;

namespace Chiro.CiviCrm.Api
{
    public partial interface ICiviCrmApi
    {
        /// <summary>
        /// Probeert een membership aan te maken, of past een bestaande waar nodig aan.
        /// </summary>
        /// <param name="apiKey"></param>
        /// <param name="key"></param>
        /// <param name="request"></param>
        /// <returns></returns>
		[OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "?api_key={apiKey}&key={key}&entity=FormProcessor&action=toevoegen_chiro_aansluiting&json={request}")]
        ApiResult AansluitingVerwerken(string apiKey, string key, BaseRequest request);
    }
}
