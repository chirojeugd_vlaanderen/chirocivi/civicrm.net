Api Explorer
============

Als je genoeg rechten hebt op je CiviCRM-instantie, dan bestaat er een
handige tool om opdrachten naar de API te sturen: de CiviCRM API
explorer. Je vindt hem op
https://server.example.org/civicrm/api/explorer
