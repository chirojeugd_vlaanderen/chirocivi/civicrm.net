Permissies van de API user
==========================

De API user moet in principe de juiste gebruikersrechten hebben om
toegang te hebben tot CiviCRM. Die gebruikersrechten zijn gezet op de
rol van de Drupal-user die overeenkomt met je CiviCRM-API-user. Typisch
kun je die gebruikersrechten toekennen of afnemen via
https://server.example.org/admin/people/permissions.
