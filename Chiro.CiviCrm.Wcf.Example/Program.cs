﻿/*
   Copyright 2013-2015, 2018 Chirojeugd-Vlaanderen vzw

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using Newtonsoft.Json.Linq;

using Chiro.CiviCrm.Api;
using Chiro.CiviCrm.Api.DataContracts;
using Chiro.CiviCrm.Api.DataContracts.Entities;
using Chiro.CiviCrm.Api.DataContracts.Filters;
using Chiro.CiviCrm.Api.DataContracts.Requests;
using Chiro.CiviCrm.Wcf.Example.Properties;
using Chiro.CiviSync.ServiceContracts.DataContracts;

namespace Chiro.CiviCrm.Wcf.Example
{
    /// <summary>
    /// Examples for the CiviCrm-API proof of concept.
    /// </summary>
    /// <remarks>
    /// Please check the configuration in the App.config file of this project.
    /// You should adapt the endpoint address of your API, and your site key
    /// and api key in the settings.
    /// </remarks>
    class Program
    {
        // Put an existing external ID here:
        private const string ExternalId = "60115";
        private const string TestMailadres = "bart.boone@chiro.be";
        private const string TestStamNr = "WG /0901";

        // Get API key and site key from configuration.
        private static readonly string ApiKey = Settings.Default.ApiKey;
        private static readonly string SiteKey = Settings.Default.SiteKey;

        // Channel factory

        private static ChannelFactory<ICiviCrmApi> _factory;

        /// <summary>
        /// Choose any example you like to run.
        /// </summary>
        /// <param name="arg"></param>
        static void Main(string[] arg)
        {
            // Just use any usable endpoint in the config file.
            _factory = new ChannelFactory<ICiviCrmApi>("*");

            Console.WriteLine("Starting up ...");

            Console.WriteLine($"SiteKey: {SiteKey}");
            Console.WriteLine($"ApiKey:  {ApiKey}");

            ContactOpzoekingenTesten();

            // MembershipOpslaanTesten();
            // GratisMembershipOverschrijvenTesten();

            // RelatieOpzoekingenTesten();
            // RelatieOpslaanTesten();

            // BivakopzoekingTesten();

            // BivakOpslaanTesten();

            _factory.Close();
            Console.WriteLine("Press enter.");
            Console.ReadLine();
        }

        private static void RelatieSchrijvenTesten()
        {
            if (DoorgaanNaWaarschuwing() == true)
            {
                Console.WriteLine();

                using (var client = _factory.CreateChannel())
                {
                    var request = new ContactSaveRequest
                    {
                        ContactType = ContactType.Individual,
                        ExternalIdentifier = ExternalId,
                        FirstName = "Bart",
                        LastName = "Boone",
                        ApiOptions = new ApiOptions { Match = "external_identifier" }
                    };

                    var result = client.ContactSave(ApiKey, SiteKey, request);

                    if (result.IsError == 1)
                    {
                        Console.WriteLine(result.ErrorMessage);
                    }
                }
            }
        }

        static void BivakOpslaanTesten()
        {
            if (DoorgaanNaWaarschuwing() == false)
            {
                return;
            }

            using (var client = _factory.CreateChannel())
            {
                // Setup
                var bivak = new Bivak();
                bivak.DatumVan = new DateTime(2023, 7, 3);
                bivak.DatumTot = new DateTime(2023, 7, 12);
                bivak.StamNummer = "WG /0901";
                bivak.Naam = "Kamp 2023";
                bivak.Opmerkingen = "Nog eens aangepaste opmerking (test CiviCrm.Wcf in workaround-json-array-problem)";
                bivak.UitstapID = 37802;

                var resultaat = client.ContactGet(ApiKey, SiteKey,
                            new ContactGetRequest
                            {
                                ExternalIdentifierFilter = new Filter<string>(WhereOperator.Eq, bivak.StamNummer),
                                ReturnFields = "id"
                            });

                AssertValid(resultaat);

                if (resultaat.Count == 0)
                {
                    return;
                }

                var contact = resultaat.Values.First();

                // Overgenomen uit CiviSync
                Event oudEvent;
                int? contactIdPloeg = contact.Id;

                if (contactIdPloeg == null)
                {
                    return;
                }

                // Haal bivak op, met OrganiserendePloeg1Id.
                var apiResult = client.EventGet(ApiKey, SiteKey,
                            new EventRequest { GapUitstapId = bivak.UitstapID, ReturnFields = "id,custom_48" });

                AssertValid(apiResult);

                if (apiResult.Count == 0)
                {
                    oudEvent = null;
                }
                else
                {
                    oudEvent = apiResult.Values.First();
                }

                var eventRequest = new EventRequest
                {
                    OrganiserendePloeg1Id = contactIdPloeg
                };

                var mappingHelper = new ExampleMappingHelper();
                mappingHelper.Map(bivak, eventRequest);

                if (oudEvent != null && oudEvent.Id != 0)
                {
                    // Een stamnummer van een bivakaangifte mag niet veranderen.
                    Debug.Assert(oudEvent.OrganiserendePloeg1Id == contactIdPloeg);

                    // Als er al een event bestond, dan nemen we het ID over. De API-call overschrijft
                    // straks enkel wat gegeven is in 'bivak'.
                    eventRequest.Id = oudEvent.Id;

                    Console.WriteLine($"Bestaande bivakaangifte bijwerken voor {bivak.StamNummer.Trim()}: {bivak.Naam}. Gap-ID {bivak.UitstapID}, Civi-ID {eventRequest.Id}.",
                        bivak.StamNummer, null, null);
                }

                // Overschrijf nieuwe/bestaande met aangeleverde informatie.

                var result = client.EventSave(ApiKey, SiteKey, eventRequest);

                AssertValid(result);

                Console.WriteLine($"Bivakaangifte bewaard voor {bivak.StamNummer.Trim()}: {bivak.Naam} bewaard. Gap-ID {bivak.UitstapID}.",
                    bivak.StamNummer, null, null);
            }
        }

        /// <summary>
        /// Waarschuwen dat we gegevens gaan wegschrijven en dat je dus alleen de productie-omgeving mag gebruiken als je weet wat je doet.
        /// </summary>
        /// <returns></returns>
        private static bool DoorgaanNaWaarschuwing()
        {
            Console.WriteLine("CAREFUL! This method writes data. Are you sure you are using the right endpoint (y/n)?");

            if (Console.ReadKey().Key.ToString().ToUpper() != "Y")
            {
                Console.WriteLine(); // zorgen dat de output niet slordig aan elkaar plakt
                return false;
            }
            Console.WriteLine(); // zorgen dat de output niet slordig aan elkaar plakt
            return true;
        }

        static void RelatieOpslaanTesten()
        {
            // Setup
            var gedoe = new LidGedoe
            {
                StamNummer = TestStamNr,
                EindeInstapPeriode = DateTime.Now.AddDays(-1),
                Werkjaar = 2022,
                LidType = LidTypeEnum.Leiding,
                NationaleFuncties = new List<FunctieEnum> { FunctieEnum.FinancieelVerantwoordelijke },
                OfficieleAfdelingen = new List<AfdelingEnum> { AfdelingEnum.Speelclub }
            };

            // aangepaste method

            int? civiGroepId = ContactIdGet(TestStamNr);

            if (civiGroepId == null)
            {
                Console.WriteLine($"Onbestaande groep - lid niet bewaard. Stamnr: {TestStamNr.Trim()}.");

                return;
            }
            else
            {
                Console.WriteLine($"Groep opgehaald ifv LidBewaren: {TestStamNr.Trim()}.");
            }

            // We halen het recentste lid op, en niet het 'actieve' lid, zodat we zo nodig een inactief
            // lid weer kunnen activeren.
            var contact = PersoonMetRecentsteLid(int.Parse(ExternalId), civiGroepId);

            if (contact == null || contact.ExternalIdentifier == null)
            {
                Console.WriteLine($"Onbestaand AD-nummer voor te bewaren lid - als dusdanig terug naar GAP: {ExternalId}.");

                return;
            }
            else
            {
                Console.WriteLine($"Contact opgehaald ifv LidBewaren: AD-nr {ExternalId}  met {contact.RelationshipResult.Count} relatie(s).");
            }

            // Request voor te bewaren (nieuwe) lidrelatie: eerst een standaardrequest voor dit werkjaar.
            // Als het contact al zo'n relatie had (contact.RelationshipResult), dan nemen we van die bestaande
            // de relevante zaken over.
            var relationshipRequest = RelationshipLogic.RequestMaken(RelatieType.LidVan, contact.Id, civiGroepId.Value, gedoe.Werkjaar,
                gedoe.UitschrijfDatum);

            if (contact.RelationshipResult.Count == 1)
            {
                var bestaandeRelatie = contact.RelationshipResult.Values.First();

                // Het zou cool zijn mochten we gewoon kunnen zeggen: er bestaat nog geen actieve relatie, we
                // maken er een nieuwe. Maar op die manier zou de civi vol met superkorte relaties raken als
                // iemand met een slecht karakter een van hun leden constant in- en uitschrijft.
                // Het kan dus zijn dat een gestopte relatie toch als huidig wordt beschouwd, namelijk als ze
                // nog niet zo lang geleden is gestart.
                if (RelationshipLogic.IsHuidig(bestaandeRelatie))
                {
                    if (bestaandeRelatie.Werkjaar == gedoe.Werkjaar)
                    {
                        // Neem van bestaande relatie het ID en de begindatum over.
                        // Dat maakt dat we de bestaande relatie gaan bewerken, dus moet de einddatum
                        // voor de zekerheid ook leeggemaakt worden.
                        relationshipRequest.Id = bestaandeRelatie.Id;
                        relationshipRequest.StartDate = bestaandeRelatie.StartDate;

                        if (bestaandeRelatie.IsActive)
                        {
                            Console.WriteLine(string.Concat($"Bestaand lidobject wordt geupdatet. {contact.FirstName} {contact.LastName} ",
                                                      $"(AD {contact.ExternalIdentifier}) was al lid voor groep {gedoe.StamNummer.Trim()}; ",
                                                      $"startdatum {bestaandeRelatie.StartDate}."));
                        }
                        else
                        {
                            Console.WriteLine(string.Concat($"Inactieve lidrelatie wordt bijgewerkt: van {contact.FirstName} {contact.LastName} ",
                                                      $"(AD {ExternalId}) voor groep {gedoe.StamNummer.Trim()} met startdatum {bestaandeRelatie.StartDate}."));
                        }
                    }
                    else
                    {
                        // Bestaande relatie afsluiten.
                        // LidUitschrijven(ExternalId, gedoe.StamNummer, DateTime.Now.AddDays(-1));

                        Console.WriteLine(string.Concat($"Actieve maar verlopen lidrelatie beeindigd van {contact.FirstName} {contact.LastName} ",
                                                  $"(AD {ExternalId}) voor groep {gedoe.StamNummer.Trim()} met startdatum {bestaandeRelatie.StartDate}."));
                    }
                }
            }

            var mappingHelper = new ExampleMappingHelper();

            // We vervangen functies en afdelingen.
            relationshipRequest.Afdeling = gedoe.LidType == LidTypeEnum.Kind
                ? mappingHelper.Map<AfdelingEnum, Afdeling>(gedoe.OfficieleAfdelingen.First()) : Afdeling.Leiding;

            relationshipRequest.LeidingVan = gedoe.LidType == LidTypeEnum.Leiding
                ? mappingHelper.Map<IEnumerable<AfdelingEnum>, Afdeling[]>(gedoe.OfficieleAfdelingen)
                : null;

            relationshipRequest.Functies = FunctieLogic.KipCodes(gedoe.NationaleFuncties);

            using (var client = _factory.CreateChannel())
            {
                var result = client.RelationshipSave(ApiKey, SiteKey, relationshipRequest);

                AssertValid(result);

                Console.WriteLine(string.Concat(gedoe.UitschrijfDatum == null ? "Inschrijving " : "Uitschrijving ",
                                          $"{contact.FirstName} {contact.LastName}: AD {ExternalId} stamnr {gedoe.StamNummer.Trim()} ",
                                          $"start {relationshipRequest.StartDate:dd/MM/yyyy} - ",
                                          $"afd {relationshipRequest.Afdeling}, lafd ",
                                          relationshipRequest.LeidingVan == null
                                                                            ? "n/a"
                                                                            : string.Join(",", relationshipRequest.LeidingVan.Select(afd => afd.ToString())),
                                          $", func ",
                                          relationshipRequest.Functies == null
                                                                          ? "(geen)"
                                                                          : string.Join(",", relationshipRequest.Functies),
                                          $" relID {result.Id}"));
            }
        }

        static void MembershipOpslaanTesten()
        {
            var request = new FormProcessorRequest()
            {
                StartDate = new DateTime(2023, 9, 30),
                LidContactId = 11301,
                PloegContactId = 487237,
                Status = FactuurStatus.VolledigTeFactureren,
                IsGratis = true
            };

            using (var client = _factory.CreateChannel())
            {
                var result = client.AansluitingVerwerken(ApiKey, SiteKey, request);

                if (result.IsError == 0)
                {
                    Console.WriteLine("Gratis membership verwerkt (ofwel geregistreerd, ofwel genegeerd wegens aanwezig betalend membership.");
                }
                else
                {
                    Console.WriteLine(result.ErrorMessage);
                }
            }
        }

        static void GratisMembershipOverschrijvenTesten()
        {
            // Eerst een gratis membership opslaan

            MembershipOpslaanTesten();

            // Nu vervangen door een betalend membership

            var request = new FormProcessorRequest()
            {
                StartDate = new DateTime(2023, 9, 30),
                LidContactId = 11301, // Geert Buyse
                PloegContactId = 346364, // Chiro Bavikhove
                Status = FactuurStatus.VolledigTeFactureren,
                IsGratis = false,
                ExtraVerzekering = false
            };

            using (var client = _factory.CreateChannel())
            {
                var result = client.AansluitingVerwerken(ApiKey, SiteKey, request);

                if (result.IsError == 0)
                {
                    Console.WriteLine("Gratis membership veranderd in een betalend.");
                }
                else
                {
                    Console.WriteLine(result.ErrorMessage);
                }
            }
        }

        #region opzoekingen 

        static void ContactOpzoekingenTesten()
        {
            using (var client = _factory.CreateChannel())
            {
                // Get the contact, and chain the contact's addresses.
                var contact = client.ContactGet(ApiKey, SiteKey, new ContactGetRequest
                {
                    ExternalIdentifierFilter = new Filter<string>(WhereOperator.Eq, ExternalId)
                });

                // Exit if contact is not found.
                if (contact == null)
                {
                    if (contact.IsError == 1) { Console.WriteLine(contact.ErrorMessage); };

                    Console.WriteLine("Contact not found.");
                    return;
                }
                else if (contact.IsError == 1)
                {
                    Console.WriteLine(contact.ErrorMessage);

                    if (contact.ErrorMessage.Contains("authentication"))
                    {
                        Console.WriteLine("Kijk de API-key na op https://civistaging.chiro.be/civicrm/contact/view?reset=1&cid=346514&selectedChild=apiKey (moet beginnen met ahHo).");
                    }

                    return;
                };

                ShowContact(contact.Values[0]);

                var mail = client.EmailGet(ApiKey, SiteKey, new EmailRequest { EmailAddress = TestMailadres });
                Console.WriteLine("Mailrequest: {0}", mail.Values[0].EmailAddress);

                var contactMetMail = client.ContactGet(ApiKey, SiteKey, new ContactGetRequest
                {
                    ExternalIdentifierFilter = new Filter<string>(WhereOperator.Eq, ExternalId),
                    EmailGetRequest = new EmailRequest()
                });

                if (contactMetMail.Values != null
                    && contactMetMail.Values[0].ExternalIdentifier != null
                    && contactMetMail.Values[0].EmailResult.Values != null
                    && contactMetMail.Values[0].EmailResult.Values[0] != null)
                {
                    Console.WriteLine("Chained mailrequest: {0}", contactMetMail.Values[0].EmailResult.Values[0].EmailAddress);
                }
                else
                {
                    if (contactMetMail.IsError == 1) { Console.WriteLine(contactMetMail.ErrorMessage); };

                    Console.WriteLine("Chained mailrequest failed");
                }
            }
        }

        static void RelatieOpzoekingenTesten()
        {
            using (var client = _factory.CreateChannel())
            {
                // Get the contact, and chain the contact's addresses.
                var contact = client.ContactGet(ApiKey, SiteKey, new ContactGetRequest
                {
                    ExternalIdentifierFilter = new Filter<string>(WhereOperator.Eq, ExternalId)
                });

                // Exit if contact is not found.
                if (contact == null)
                {
                    if (contact.IsError == 1) { Console.WriteLine(contact.ErrorMessage); };

                    Console.WriteLine("Contact not found.");
                    return;
                }

                ShowContact(contact.Values[0]);

                var persoonMetRelaties = client.ContactGet(ApiKey, SiteKey, new ContactGetRequest
                {
                    ExternalIdentifierFilter = new Filter<string>(WhereOperator.Eq, ExternalId),
                    ReturnFields = "id,custom_78", //custom_78 = werkjaar
                    RelationshipGetRequest = new RelationshipRequest
                    {
                        ContactIdAValueExpression = "$value.id",
                        ReturnFields = "id,contact_id_a,contact_id_b",
                        RelationshipTypeId = (int)RelatieType.LidVan,
                        IsActive = true,
                        ApiOptions = new ApiOptions { Limit = 0 },
                        ContactGetRequest = new ContactRequest
                        {
                            IdValueExpression = "$value.contact_id_b",
                            ApiOptions = new ApiOptions { Limit = 0 },
                            ReturnFields = "external_identifier,display_name,custom_2" // custom_2 = kaderniveau
                        }
                    }
                });

                if (persoonMetRelaties != null && persoonMetRelaties.Values[0].RelationshipResult.Values != null)
                {
                    foreach (var relatie in persoonMetRelaties.Values[0].RelationshipResult.Values)
                    {
                        ShowRelationship(relatie);
                    }
                }
                else
                {
                    if (persoonMetRelaties.IsError == 1) { Console.WriteLine(persoonMetRelaties.ErrorMessage); };

                    Console.WriteLine("No relationships found.");
                }
            }
        }

        static void BivakopzoekingTesten()
        {
            using (var client = _factory.CreateChannel())
            {
                int jaar = DateTime.Now.Year;
                var periodeStart = new DateTime(jaar, 7, 1);
                var periodeEinde = new DateTime(jaar, 8, 31);

                var request = new EventRequest
                {
                    EndDate = new Filter<DateTime?>(WhereOperator.Gte, periodeStart),
                    StartDate = new Filter<DateTime?>(WhereOperator.Lte, periodeEinde),
                    // BIVAK
                    EventTypeId = 100,
                    CourseResponsableId = new Filter<int?>(WhereOperator.IsNotNull),
                    LocBlockIdFilter = new Filter<int>(WhereOperator.IsNotNull),
                    // Organiserende ploeg 1 en GAP-uitstapID
                    ReturnFields = "title,custom_48,custom_53",
                    ApiOptions = new ApiOptions { Limit = 30 }
                };

                var civiResult = client.EventGet(ApiKey, SiteKey, request);

                if (civiResult != null && civiResult.Values != null)
                {
                    foreach (var bivakaangifte in civiResult.Values)
                    {
                        ShowEvent(bivakaangifte);
                    }
                }
                else
                {
                    if (civiResult.IsError == 1)
                    {
                        Console.WriteLine(civiResult.ErrorMessage);
                    };

                    Console.WriteLine("No events found.");
                }
            }
        }

        #endregion

        #region examples

        /// <summary>
        /// This example gets a contact, and shows its info.
        /// </summary>
        static void Example0()
        {
            using (var client = _factory.CreateChannel())
            {
                // Get the contact, and chain the contact's addresses.
                var contact = client.ContactGetSingle(ApiKey, SiteKey, new ContactGetRequest
                {
                    ExternalIdentifierFilter = new Filter<string>(WhereOperator.Eq, ExternalId)
                });

                // Exit if contact is not found.
                if (contact == null)
                {
                    Console.WriteLine("Contact not found.");
                    return;
                }

                ShowContact(contact);
            }
        }

        /// <summary>
        /// Gets a contact with the generic GetSingle.
        /// </summary>
        static void Example0_1()
        {
            using (var client = _factory.CreateChannel())
            {
                var result =
                    client.GetSingle(ApiKey, SiteKey, CiviEntity.Contact,
                        new ContactGetRequest { ExternalIdentifierFilter = new Filter<string>(WhereOperator.Eq, ExternalId) }) as JObject;

                if (result == null)
                {
                    Console.WriteLine("Contact not found.");
                    return;
                }
                var contact = result.ToObject<Contact>();
                ShowContact(contact);
            }
        }

        /// <summary>
        /// This example gets a contact, adds an address, and deletes the
        /// address again.
        /// </summary>
        static void Example1()
        {
            using (var client = _factory.CreateChannel())
            {
                // Get the contact, and chain the contact's addresses.
                var contact = client.ContactGetSingle(ApiKey, SiteKey, new ContactGetRequest
                {
                    ExternalIdentifierFilter = new Filter<string>(WhereOperator.Eq, ExternalId),
                    AddressGetRequest = new AddressRequest()
                });

                // Keep the contact Id for later reference.
                int contactId = contact.Id;

                // Exit if contact is not found.
                if (contact == null)
                {
                    Console.WriteLine("Contact not found.");
                    return;
                }

                ShowContact(contact);
                ShowAddresses(contact);

                // Add an address to the contact.
                var newAddress = new AddressRequest
                {
                    ContactId = contact.Id,
                    StreetAddress = "Hoefslagstraatje 2",
                    PostalCode = "9000",
                    City = "Gent",
                    Country = "BE",
                    LocationTypeId = 1,
                };

                var result = client.AddressSave(ApiKey, SiteKey, newAddress);
                Debug.Assert(result.Id != null);
                newAddress.Id = result.Id.Value;

                // Get contact again, to find out whether the address 
                // has been added.
                // Note that we now use the CiviCRM contact ID.
                contact = client.ContactGetSingle(ApiKey, SiteKey, new ContactGetRequest
                {
                    Id = contactId,
                    // We don't need all fields of the contact, we are only interested in the
                    // addresses.

                    // ReturnFields are still in civicrm notation, meaning lowercase and
                    // underscores (see issue #19)
                    ReturnFields = "id",
                    AddressGetRequest = new AddressRequest()
                });

                // Show adresses
                ShowAddresses(contact);

                // Delete the added addres
                client.AddressDelete(ApiKey, SiteKey, new DeleteRequest(newAddress.Id.Value));

                // Get the adresses again, to verify that the new address is gone.
                contact = client.ContactGetSingle(ApiKey, SiteKey, new ContactGetRequest
                {
                    Id = contactId,
                    ReturnFields = "id",
                    AddressGetRequest = new AddressRequest()
                });

                ShowAddresses(contact);
            }
        }

        /// <summary>
        /// This example changes name and birth date of a contact.
        /// </summary>
        public static void Example2()
        {
            using (var client = _factory.CreateChannel())
            {
                // Get the contact, and chain the contact's addresses.
                var contact = client.ContactGetSingle(ApiKey, SiteKey, new ContactGetRequest
                {
                    ExternalIdentifierFilter = new Filter<string>(WhereOperator.Eq, ExternalId),
                    AddressGetRequest = new AddressRequest()
                });

                // Exit if contact is not found.
                if (contact == null)
                {
                    Console.WriteLine("Contact not found.");
                    return;
                }

                // Keep the contact Id for later reference.
                int contactId = contact.Id;

                ShowContact(contact);

                // Change first name and birth date:
                contact.FirstName = "Jos";
                contact.BirthDate = new DateTime(1979, 3, 3);
                client.ContactSave(ApiKey, SiteKey,
                    new ContactSaveRequest { Id = contactId, FirstName = "Jos", BirthDate = new DateTime(1979, 3, 3) });

                // Get contact again, to see whether it has worked.
                contact = client.ContactGetSingle(ApiKey, SiteKey, new ContactGetRequest
                {
                    ExternalIdentifierFilter = new Filter<string>(WhereOperator.Eq, ExternalId),
                    AddressGetRequest = new AddressRequest()
                });
                ShowContact(contact);
            }
        }

        /// <summary>
        /// This example updates a contact with a given external ID, without
        /// knowing the CiviCRM ID.
        /// </summary>
        public static void Example3()
        {
            using (var client = _factory.CreateChannel())
            {
                var contactRequest = new ContactSaveRequest
                {
                    ExternalIdentifier = ExternalId,
                    FirstName = "Wesley",
                    LastName = "Decabooter",
                    // use external ID to find the contact, instead of contact id.
                    ApiOptions = new ApiOptions { Match = "external_identifier" }
                };

                client.ContactSave(
                    ApiKey, SiteKey, contactRequest
                    );

                // Get the contact again. First name and last name
                // should be updated. Other info should still be
                // there.

                var contact = client.ContactGetSingle(ApiKey, SiteKey, new ContactGetRequest { ExternalIdentifierFilter = new Filter<string>(WhereOperator.Eq, ExternalId) });
                ShowContact(contact);
            }
        }

        /// <summary>
        ///  Changes gender and preferred mail format, as test for enums.
        /// </summary>
        public static void Example4()
        {
            using (var client = _factory.CreateChannel())
            {
                var contact = client.ContactGetSingle(ApiKey, SiteKey,
                    new ContactGetRequest { ExternalIdentifierFilter = new Filter<string>(WhereOperator.Eq, ExternalId) });
                ShowContact(contact);

                contact.Gender = contact.Gender == Gender.Male ? Gender.Female : Gender.Male;
                contact.PreferredMailFormat = contact.PreferredMailFormat == MailFormat.HTML ? MailFormat.Text : MailFormat.HTML;

                var result = client.ContactSave(
                    ApiKey, SiteKey, new ContactSaveRequest
                    {
                        Id = contact.Id,
                        Gender = contact.Gender == Gender.Male ? Gender.Female : Gender.Male,
                        PreferredMailFormat =
                            contact.PreferredMailFormat == MailFormat.HTML ? MailFormat.Text : MailFormat.HTML
                    }
                    );

                // Get contact again to check.

                contact = client.ContactGetSingle(ApiKey, SiteKey,
                    new ContactGetRequest { ExternalIdentifierFilter = new Filter<string>(WhereOperator.Eq, ExternalId) });
                ShowContact(contact);
            }
        }

        /// <summary>
        /// Create a new contact with an external ID.
        /// </summary>
        public static void Example5()
        {
            using (var client = _factory.CreateChannel())
            {
                var contact = new ContactSaveRequest
                {
                    ContactType = ContactType.Individual,
                    FirstName = "Lucky",
                    LastName = "Luke",
                    BirthDate = new DateTime(1946, 3, 3),
                    Gender = Gender.Male,
                    ExternalIdentifier = "YADAYADA",
                    ApiOptions = new ApiOptions { Match = "external_identifier" }
                };
                var result = client.ContactSave(ApiKey, SiteKey, contact);

                ShowContact(result.Values.FirstOrDefault());

                client.ContactDelete(ApiKey, SiteKey, new DeleteRequest(result.Id.Value), 1);
            }
        }

        /// <summary>
        /// Website example.
        /// </summary>
        public static void Example6()
        {
            using (var client = _factory.CreateChannel())
            {
                var contactResult = client.ContactGet(ApiKey, SiteKey,
                    new ContactGetRequest { ExternalIdentifierFilter = new Filter<string>(WhereOperator.Eq, ExternalId) });

                var website = new WebsiteRequest
                {
                    ContactId = contactResult.Id,
                    Url = "http://blog.johanv.org",
                    WebsiteType = WebsiteType.Main
                };

                var result = client.WebsiteSave(ApiKey, SiteKey, website);
                website.Id = result.Values.First().Id;

                Console.WriteLine("Website added for contact with ContactID {0}  (external ID {1})", contactResult.Id, ExternalId);
                Console.WriteLine("You might want to check that. Then press enter.");
                Console.ReadLine();

                Debug.Assert(website.Id != null);
                client.WebsiteDelete(ApiKey, SiteKey, new DeleteRequest(website.Id.Value));

                Console.WriteLine("Website was deleted again.");
            }
        }

        /// <summary>
        /// Get a contact with all communication.
        /// </summary>
        public static void Example7()
        {
            using (var client = _factory.CreateChannel())
            {
                var contact = client.ContactGetSingle(ApiKey, SiteKey,
                    new ContactGetRequest
                    {
                        ExternalIdentifierFilter = new Filter<string>(WhereOperator.Eq, ExternalId),
                        PhoneGetRequest = new PhoneRequest(),
                        EmailGetRequest = new EmailRequest(),
                        ImGetRequest = new ImRequest(),
                        WebsiteGetRequest = new WebsiteRequest(),
                    });
                ShowContact(contact);
                ShowCommunication(contact);
            }
        }

        /// <summary>
        /// Chained writing
        /// </summary>
        public static void Example8()
        {
            using (var client = _factory.CreateChannel())
            {
                // Create a contact, chain website.
                var result = client.ContactSave(ApiKey, SiteKey,
                    new ContactSaveRequest
                    {
                        ContactType = ContactType.Individual,
                        LastName = "Smurf",
                        FirstName = "Smul",
                        WebsiteSaveRequest = new List<WebsiteRequest> { new WebsiteRequest { Url = "http://smurf.com" } }
                    });
                Debug.Assert(result.Id.HasValue);

                // Get contact with websites
                var contact = client.ContactGetSingle(ApiKey, SiteKey,
                    new ContactGetRequest
                    {
                        Id = result.Id.Value,
                        WebsiteGetRequest = new WebsiteRequest()
                    });

                ShowContact(contact);
                ShowCommunication(contact);

                // Delete contact

                client.ContactDelete(ApiKey, SiteKey, new DeleteRequest(result.Id.Value), 1);
            }
        }

        /// <summary>
        /// Relationship example
        /// </summary>
        public static void Example9()
        {
            using (var client = _factory.CreateChannel())
            {
                var result = client.RelationshipGet(ApiKey, SiteKey, new RelationshipRequest { Id = 12388 });
                var relationship = result.Values.FirstOrDefault();

                if (relationship == null)
                {
                    Console.WriteLine("Relationship not found.");
                }
                else
                {
                    Console.WriteLine("Contact ID a: {0}", relationship.ContactIdA);
                    Console.WriteLine("Contact ID b: {0}", relationship.ContactIdB);
                    Console.WriteLine("Start date: {0}", relationship.StartDate);
                    Console.WriteLine("End date: {0}", relationship.EndDate);
                }
            }
        }

        /// <summary>
        /// Participant example
        /// </summary>
        private static void Example10()
        {
            using (var client = _factory.CreateChannel())
            {
                var result = client.ContactGetSingle(ApiKey, SiteKey, new ContactGetRequest
                {
                    ExternalIdentifierFilter = new Filter<string>(WhereOperator.Eq, ExternalId),
                    ReturnFields = "id",
                    ParticipantGetRequest = new ParticipantRequest
                    {
                        ApiOptions = new ApiOptions { Limit = 0 }
                    }
                });

                if (result.ParticipantResult.Values.Length > 0)
                {
                    foreach (Participant participant in result.ParticipantResult.Values)
                    {
                        Console.WriteLine("Participant ({0}): {1}", participant.Id, participant.EventTitle);
                    }
                }
                else
                {
                    Console.WriteLine("No Participant results found."); ;
                }
            }
        }

        #endregion

        #region some output functions.

        private static void ShowContact(Contact contact)
        {
            // Show some information about the contact.
            Console.WriteLine("Found: {0} {1} ({4}); id: {2}; {3}", contact.FirstName, contact.LastName, contact.Id, contact.ContactType, contact.Gender);
            Console.WriteLine("Birth date: {0}", contact.BirthDate);
            Console.WriteLine("Deceased date: {0}", contact.DeceasedDate);
            Console.WriteLine("External ID: {0}", contact.ExternalIdentifier);
            Console.WriteLine("Mail Format: {0}", contact.PreferredMailFormat);
        }

        private static void ShowAddresses(Contact c)
        {
            Console.WriteLine("\nAddresses:");
            foreach (var a in c.AddressResult.Values)
            {
                Console.WriteLine(
                    "  Address {0}: {1}, {2} {5} {3} - {4},{6}",
                    a.Id,
                    a.StreetAddress,
                    a.PostalCode,
                    a.City,
                    a.CountryId,
                    a.PostalCodeSuffix,
                    a.StateProvinceId);
            }
        }

        private static void ShowCommunication(Contact contact)
        {
            if (contact.PhoneResult != null && contact.PhoneResult.Count > 0)
            {
                foreach (var p in contact.PhoneResult.Values)
                {
                    Console.WriteLine("Phone ({0}): {1}", p.PhoneType, p.PhoneNumber);
                }
            }
            if (contact.EmailResult != null && contact.EmailResult.Count > 0)
            {
                foreach (var e in contact.EmailResult.Values)
                {
                    Console.WriteLine("E-mail ({0}): {1}", e.LocationTypeId, e.EmailAddress);
                }
            }
            if (contact.WebsiteResult != null && contact.WebsiteResult.Count > 0)
            {
                foreach (var w in contact.WebsiteResult.Values)
                {
                    Console.WriteLine("Website ({0}): {1}", w.WebsiteType, w.Url);
                }
            }
            if (contact.ImResult != null && contact.ImResult.Count > 0)
            {
                foreach (var im in contact.ImResult.Values)
                {
                    Console.WriteLine("{0}: {1}", im.Provider, im.Name);
                }
            }
        }

        private static void ShowRelationship(Relationship relatie)
        {
            Console.WriteLine("Relation: {0}", relatie.ContactResult.Values[0].DisplayName);
        }

        private static void ShowEvent(Event bivakaangifte)
        {
            Console.WriteLine("Bivak: {0}", bivakaangifte.Title);
        }

        #endregion

        private static void AssertValid(ApiResult result)
        {
            if (result == null)
            {
                throw new ArgumentNullException("result");
            }
            if (result.IsError > 0)
            {
                throw new InvalidOperationException(result.ErrorMessage);
            }
        }

        private static int? ContactIdGet(string externalIdentifier)
        {
            using (var client = _factory.CreateChannel())
            {
                var result = client.ContactGet(ApiKey, SiteKey,
                            new ContactGetRequest { ExternalIdentifierFilter = new Filter<string>(WhereOperator.Eq, externalIdentifier), ReturnFields = "id" });

                AssertValid(result);

                if (result.Count == 0)
                {
                    return null;
                }

                var contact = result.Values.First();
                return contact.Id;
            }
        }

        private static Contact PersoonMetRecentsteLid(int adNummer, int? civiGroepID)
        {
            using (var client = _factory.CreateChannel())
            {
                // Haal de persoon op met gegeven AD-nummer en zijn of haar recentste lidrelatie in de gevraagde groep.
                var contactRequest = new ContactGetRequest
                {
                    ExternalIdentifierFilter = new Filter<string>(WhereOperator.Eq, adNummer.ToString()),
                    ContactType = ContactType.Individual,
                    RelationshipGetRequest = new RelationshipRequest
                    {
                        RelationshipTypeId = (int)RelatieType.LidVan,
                        ContactIdAValueExpression = "$value.id",
                        ContactIdB = civiGroepID,
                        ApiOptions = new ApiOptions { Sort = "start_date DESC", Limit = 1 }
                    }
                };

                var result = client.ContactGet(ApiKey, SiteKey, contactRequest);

                AssertValid(result);

                return result.Count == 0 ? null : result.Values.First();
            }
        }
    }
}
