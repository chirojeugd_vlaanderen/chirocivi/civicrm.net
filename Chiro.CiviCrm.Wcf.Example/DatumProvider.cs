﻿using System;

namespace Chiro.CiviCrm.Wcf.Example
{
    /// <summary>
    /// Een helper om de datum van vandaag op te vragen. Door deze te gebruiken i.p.v. DateTime.Now(), kunnen we
    /// die datum vervangen bij het testen.
    /// </summary>
    public static class DatumProvider
    {
        /// <summary>
        /// Levert de datum van vandaag op (zonder tijdscomponent)
        /// </summary>
        /// <returns>De datum van vandaag.</returns>
        public static DateTime Vandaag()
        {
            return DateTime.Now.Date;
        }
    }
}
