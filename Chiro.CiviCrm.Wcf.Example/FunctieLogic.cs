﻿using Chiro.CiviSync.ServiceContracts.DataContracts;

using System.Collections.Generic;
using System.Linq;

namespace Chiro.CiviCrm.Wcf.Example
{
    public static class FunctieLogic
    {
        public static readonly IDictionary<FunctieEnum, string> KipCode = new Dictionary<FunctieEnum, string>
        {
            {FunctieEnum.ContactPersoon, "GG1"},
            {FunctieEnum.GroepsLeiding, "GG2"},
            {FunctieEnum.Vb, "GV1"},
            {FunctieEnum.FinancieelVerantwoordelijke, "FI"},
            {FunctieEnum.JeugdRaad, "JR"},
            {FunctieEnum.KookPloeg, "KK"},
            {FunctieEnum.Proost, "GP"},
            {FunctieEnum.GroepsLeidingsBijeenkomsten, "2BJ"},
            {FunctieEnum.SomVerantwoordelijke, "2SO"},
            {FunctieEnum.RibbelVerantwoordelijke, "2AP"},
            {FunctieEnum.SpeelclubVerantwoordelijke, "2AS"},
            {FunctieEnum.RakwiVerantwoordelijke, "2AR"},
            {FunctieEnum.TitoVerantwoordelijke, "2AT"},
            {FunctieEnum.KetiVerantwoordelijke, "2AK"},
            {FunctieEnum.AspiVerantwoordelijke, "2AA"},
            {FunctieEnum.SomGewesten, "3SO"},
            {FunctieEnum.OpvolgingStadsGroepen, "3VS"},
            {FunctieEnum.Verbondsraad, "3VR"},
            {FunctieEnum.Verbondskern, "3KE"},
            {FunctieEnum.AmbriageVerantwoordelijke, "3SD"},
            {FunctieEnum.AccuVerantwoordelijke, "3BS"},
            { FunctieEnum.GapVerantwoordelijke, "GAP"}
        };

        /// <summary>
        /// Zet <paramref name="functies"/> van het GAP om naar functies voor
        /// ChiroCivi, die eigenlijk bepaald worden door de codes in de oude
        /// Kipadmin.
        /// </summary>
        /// <param name="functies">Om te zetten functies.</param>
        /// <returns>Functiecodes uit ChiroCivi.</returns>
        public static string[] KipCodes(IEnumerable<FunctieEnum> functies)
        {
            return functies == null ? null : (from f in functies select KipCode[f]).ToArray();
        }
    }

}
