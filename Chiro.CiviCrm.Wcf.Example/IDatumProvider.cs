﻿using System;

namespace Chiro.CiviCrm.Wcf.Example
{
    /// <summary>
    /// Een helper om de datum van vandaag op te vragen. Door deze te gebruiken i.p.v. DateTime.Now(), kunnen we
    /// die datum vervangen bij het testen.
    /// </summary>
    public interface IDatumProvider
    {
        /// <summary>
        /// Levert de datum van vandaag op (zonder tijdscomponent)
        /// </summary>
        /// <returns>De datum van vandaag.</returns>
        DateTime Vandaag();
    }
}
