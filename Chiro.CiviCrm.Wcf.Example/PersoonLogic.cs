﻿using Chiro.CiviCrm.Api.DataContracts;
using Chiro.CiviSync.ServiceContracts.DataContracts;

namespace Chiro.CiviCrm.Wcf.Example
{
    public static class PersoonLogic
    {
        /// <summary>
        /// Zet een gegeven GAP-<paramref name="geslacht"/> om naar een gender in
        /// CiviCrm.
        /// </summary>
        /// <param name="geslacht">Om te zetten geslacht</param>
        /// <returns>CiviCRM-gender</returns>
        public static Gender? GeslachtNaarGender(GeslachtsEnum geslacht)
        {
            switch (geslacht)
            {
                case GeslachtsEnum.Man:
                    return Gender.Male;
                case GeslachtsEnum.Vrouw:
                    return Gender.Female;
                case GeslachtsEnum.X:
                    return Gender.Transgender;
                default:
                    return null;
            }
        }
    }
}
