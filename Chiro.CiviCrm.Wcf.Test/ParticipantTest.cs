﻿/*
  Copyright 2015, 2016, 2018 Chirojeugd-Vlaanderen vzw

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

using System;
using System.Linq;

using Chiro.CiviCrm.Api.DataContracts;
using Chiro.CiviCrm.Api.DataContracts.Filters;
using Chiro.CiviCrm.Api.DataContracts.Requests;

using NUnit.Framework;

namespace Chiro.CiviCrm.Wcf.Test
{
    [TestFixture]
    public class ParticipantTest
    {
        private int _myContactId;
        private int _myParticipantId;

        [SetUp]
        public void InitializeTests()
        {
            using (var client = TestHelper.ClientGet())
            {
                var result1 = client.ContactSave(TestHelper.ApiKey, TestHelper.SiteKey,
                    new ContactSaveRequest
                    {
                        FirstName = "Joe",
                        LastName = "Schmoe",
                        BirthDate = new DateTime(1980, 2, 9),
                        ContactType = ContactType.Individual
                    });

                _myContactId = result1.Values.First().Id;

            }
        }

        [TearDown]
        public void CleanupTest()
        {
            using (var client = TestHelper.ClientGet())
            {
                client.ContactDelete(TestHelper.ApiKey, TestHelper.SiteKey,
                    new DeleteRequest(_myContactId),
                    1);
            }
        }

        /// <summary>
        /// Test chaining ContactRequests.
        /// </summary>
        [Test]
        public void GetParticipantWithContact()
        {
            var participantRequest = new ParticipantRequest
            {
                ContactGetRequest = new ContactRequest(),
                ContactId = _myContactId
            };
            using (var client = TestHelper.ClientGet())
            {
                var result = client.ParticipantGet(TestHelper.ApiKey, TestHelper.SiteKey, participantRequest);

                Assert.IsTrue(result.Count >= 1);
                var first = result.Values.First();
                Assert.IsNotNull(first.ContactResult);
            }
        }
    }
}
